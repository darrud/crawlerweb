﻿using System.Threading;
using System.Threading.Tasks;
using Crawler.Common.Providers;
using Crawler.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Crawler.DAL.DatabaseContext
{
    public class ApplicationDbContext : DbContext, IDbContext
    {
        private readonly ISolutionConfigurationProvider _configuration;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ISolutionConfigurationProvider configuration) : base(options)
        {
            _configuration = configuration;
        }

        public ApplicationDbContext(ISolutionConfigurationProvider configuration)
        {
            _configuration = configuration;
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await this.SaveChangesAsync(CancellationToken.None);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetSqlServerConnectionString());
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<PeopleToMovie> PeopleToMovies { get; set; }
    }


}
