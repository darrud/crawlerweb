﻿using System.Threading.Tasks;
using Crawler.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Crawler.DAL.DatabaseContext
{
    public interface IDbContext
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();

        DbSet<Movie> Movies { get; set; }
        DbSet<Person> People { get; set; }
        DbSet<PeopleToMovie> PeopleToMovies { get; set; }
    }
}
