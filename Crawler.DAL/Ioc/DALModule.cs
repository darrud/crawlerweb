﻿using Autofac;
using Crawler.DAL.DatabaseContext;
using Crawler.DAL.Repositories;

namespace Crawler.DAL.Ioc
{
    public class DALModule :Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ApplicationDbContext>()
                .As<IDbContext>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MoviesRepository>()
                .As<IMoviesRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PeopleRepository>()
                .As<IPeopleRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PeopleToMovieRepository>()
                .As<IPeopleToMovieRepository>()
                .InstancePerLifetimeScope();
        }
    }
}
