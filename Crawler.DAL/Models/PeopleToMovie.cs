﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Crawler.DAL.Models
{
    public class PeopleToMovie
    {
        [Key]
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int MovieId { get; set; }
        public string NameInMovie { get; set; }
    }
}