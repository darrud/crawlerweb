﻿using System.ComponentModel.DataAnnotations;

namespace Crawler.DAL.Models
{
    public class Person
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public double? Rate { get; set; }
        public int FilmWebId { get; set; }
        public string Poster { get; set; }
    }
}
 