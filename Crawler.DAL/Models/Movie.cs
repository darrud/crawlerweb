﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Crawler.DAL.Models
{
    public class Movie
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public TimeSpan Length { get; set; }
        public string Country { get; set; }
        public DateTime? Premiere { get; set; }
        public string Poster { get; set; }
        public int FilmWebId { get; set; }
    }
}
