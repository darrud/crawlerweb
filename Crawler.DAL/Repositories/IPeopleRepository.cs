﻿using System.Collections.Generic;
using Crawler.DAL.Models;

namespace Crawler.DAL.Repositories
{
    public interface IPeopleRepository
    {
        IEnumerable<Person> GetAll();
        Person GetById(int id);
    }
}
