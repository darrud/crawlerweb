﻿using System.Collections.Generic;
using Crawler.DAL.Models;

namespace Crawler.DAL.Repositories
{
    public interface IMoviesRepository
    {
        IEnumerable<Movie> GetAll();
        Movie GetById(int id);
    }
}
