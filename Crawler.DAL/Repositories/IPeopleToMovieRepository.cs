﻿using System;
using System.Collections.Generic;
using System.Text;
using Crawler.DAL.Models;

namespace Crawler.DAL.Repositories
{
    public interface IPeopleToMovieRepository
    {
        IEnumerable<PeopleToMovie> GetPeopleByMovieId(int movieId);
        IEnumerable<PeopleToMovie> GetMoviesByPersonId(int personId);
    }
}
