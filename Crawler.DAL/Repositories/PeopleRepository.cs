﻿using System.Collections.Generic;
using System.Linq;
using Crawler.DAL.DatabaseContext;
using Crawler.DAL.Models;

namespace Crawler.DAL.Repositories
{
    public class PeopleRepository : IPeopleRepository
    {
        private readonly IDbContext _dbContext;

        public PeopleRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Person> GetAll()
        {
            return _dbContext.People.AsQueryable();
        }

        public Person GetById(int id)
        {
            return _dbContext.People.FirstOrDefault(x => x.Id == id);
        }
    }
}
