﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crawler.DAL.DatabaseContext;
using Crawler.DAL.Models;

namespace Crawler.DAL.Repositories
{
    public class PeopleToMovieRepository : IPeopleToMovieRepository
    {
        private readonly IDbContext _dbContext;

        public PeopleToMovieRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<PeopleToMovie> GetPeopleByMovieId(int movieId)
        {
            return _dbContext.PeopleToMovies.Where(x => x.MovieId == movieId);
        }

        public IEnumerable<PeopleToMovie> GetMoviesByPersonId(int personId)
        {
            return _dbContext.PeopleToMovies.Where(x => x.PersonId == personId);
        }
    }

}
