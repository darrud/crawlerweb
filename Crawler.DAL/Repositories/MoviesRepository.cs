﻿using System.Collections.Generic;
using System.Linq;
using Crawler.DAL.DatabaseContext;
using Crawler.DAL.Models;

namespace Crawler.DAL.Repositories
{
    public class MoviesRepository : IMoviesRepository
    {
        private readonly IDbContext _databaseContext;

        public MoviesRepository(IDbContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public IEnumerable<Movie> GetAll()
        {
            return _databaseContext.Movies.AsQueryable();
        }

        public Movie GetById(int id)
        {
            return _databaseContext.Movies.FirstOrDefault(x => x.FilmWebId == id);
        }
    }
}
