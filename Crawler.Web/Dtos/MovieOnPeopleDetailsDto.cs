﻿namespace Crawler.Web.Dtos
{
    public class MovieOnPeopleDetailsDto
    {
        public int PersonId { get; set; }
        public string NameInMovie { get; set; }
        public string Title { get; set; }

    }
}
