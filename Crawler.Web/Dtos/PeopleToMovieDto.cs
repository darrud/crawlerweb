﻿namespace Crawler.Web.Dtos
{
    public class PeopleToMovieDto
    {
        public int PersonId { get; set; }
        public int FilmWebId { get; set; }
        public string Name { get; set; }
        public string NameInMovie { get; set; }

        public PeopleToMovieDto(int personId, int filmWebId, string name, string nameInMovie)
        {
            PersonId = personId;
            FilmWebId = filmWebId;
            Name = name;
            NameInMovie = nameInMovie;
        }
        public PeopleToMovieDto() { }
    }
}
