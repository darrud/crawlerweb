﻿using Autofac;
using Crawler.Common.Ioc;
using Crawler.DAL.Ioc;
using Crawler.Web.Services;

namespace Crawler.Web.Ioc
{
    public class ContainerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<CommonModule>();
            builder.RegisterModule<DALModule>();

            builder.RegisterType<MoviesService>()
                .As<IMoviesService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PeopleService>()
                .As<IPeopleService>()
                .InstancePerLifetimeScope();
        }
    }
}
