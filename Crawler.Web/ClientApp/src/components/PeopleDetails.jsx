import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { actionCreators } from "../store/PeopleDetails";

class PeopleDetails extends Component {
  componentWillMount() {
    const personId = parseInt(this.props.match.params.id, 10);
    this.props.requestPeopleDetails(personId);
    this.props.requestPeopleMoviesDetails(personId);

  }
  render() {
    return (<div style={{padding:50}}>
      <div class="jumbotron" >
        <h2 class="display-4">{this.props.details.name}</h2>
        <div style={{height: 285}} class="lead" >
          <div class="pull-left">
          <p></p>
          <p>Rating : {this.props.details.rate}</p>
          </div>
          <img class="pull-right" src={this.props.details.poster} height="285" width="200"/>
        </div>
        <hr class="my-4" />
        <div>
          <h3>Wystąpił w :</h3>
          <br/>
        </div>
        <div class="lead">
        <ul>
              {this.props.movies.map(movie => (
                <li>
                  <Link to={`/movie/details/${movie.filmWebId}`}>
                    {movie.title}
                  </Link>
                  jako {movie.nameInMovie}
                </li>
              ))}
      </ul>
        </div>
      </div>
      </div>
    );
  }
}

export default connect(
  state => state.peopleDetails,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(PeopleDetails);
