import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/People';

class People extends Component {
  componentWillMount() {
    const pageIndex = parseInt(this.props.match.params.pageIndex, 10) || 1;
    this.props.requestPeople(pageIndex);
  }

  componentWillReceiveProps(nextProps) {
    const pageIndex = parseInt(nextProps.match.params.pageIndex, 10) || 1;
    this.props.requestPeople(pageIndex);
  }

  render() {
    return (
      <div>
        <h1>Filmy</h1>
        <p></p>
        {renderPeopleTable(this.props)}
        {renderPagination(this.props)}
      </div>
    );
  }
}

function renderPeopleTable(props) {
  return (
    <table className='table'>
      <thead>
        <tr>
          <th>Imię i Nazwisko</th>
          <th>Ocena</th>
        </tr>
      </thead>
      <tbody>
      {props.people.map(people =>
          <tr key={people.id}>
            <td><Link to={`/peopledetails/${people.id}`}>{people.name}</Link></td>
            <td>{people.rate}</td>
          </tr>
        )}
      </tbody>
    </table>
  );
}

function renderPagination(props) {
  const prevPageIndex = (props.pageIndex || 1) - 1;
  const nextPageIndex = (props.pageIndex || 1) + 1;

  return <p className='clearfix text-center'>
    <Link className='btn btn-default pull-left' to={`/people/${prevPageIndex}`}>Poprzednia</Link>
    <Link className='btn btn-default pull-right' to={`/people/${nextPageIndex}`}>Kolejna</Link>
    {props.isLoading ? <span>Loading...</span> : []}
  </p>;
}

export default connect(
  state => state.people,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(People);
