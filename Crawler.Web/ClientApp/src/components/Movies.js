import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/Movies';

class Movies extends Component {
  componentWillMount() {
    const pageIndex = parseInt(this.props.match.params.pageIndex, 10) || 1;
    this.props.requestMovies(pageIndex);
  }

  componentWillReceiveProps(nextProps) {
    const pageIndex = parseInt(nextProps.match.params.pageIndex, 10) || 1;
    this.props.requestMovies(pageIndex);
  }

  render() {
    return (
      <div>
        <h1>Filmy</h1>
        <p></p>
        {renderMoviesTable(this.props)}
        {renderPagination(this.props)}
      </div>
    );
  }
}

function renderMoviesTable(props) {
  return (
    <table className='table'>
      <thead>
        <tr>
          <th>Tytuł</th>
          <th>Kraj</th>
        </tr>
      </thead>
      <tbody>
      {props.movies.map(movie =>
          <tr key={movie.id}>
            <td><Link to={`/movie/details/${movie.filmWebId}`}>{movie.title}</Link></td>
            <td>{movie.country}</td>
          </tr>
        )}
      </tbody>
    </table>
  );
}

function renderPagination(props) {
  const prevPageIndex = (props.pageIndex || 1) - 1;
  const nextPageIndex = (props.pageIndex || 1) + 1;

  return <p className='clearfix text-center'>
    <Link className='btn btn-default pull-left' to={`/movies/${prevPageIndex}`}>Poprzednia</Link>
    <Link className='btn btn-default pull-right' to={`/movies/${nextPageIndex}`}>Kolejna</Link>
    {props.isLoading ? <span>Loading...</span> : []}
  </p>;
}

export default connect(
  state => state.movies,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Movies);
