import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { actionCreators } from "../store/PeopleDetails";
import { Redirect } from "react-router";

class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      redirectToReferrer: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ title: event.target.value, redirectToReferrer: true });
    console.log("DUPA");
  }

  render() {
    const redirectToReferrer = this.state.redirectToReferrer;
    if (redirectToReferrer === true) {
      return <Redirect to={`/movies/1/${this.state.title}`} />;
    }

    return (
      <form onSubmit={this.handleSubmit}>
        <div class="form-row align-items-center">
          <div style={{ padding: 10 }} class="col-auto">
            <input
              value={this.state.value}
              type="text"
              class="form-control mb-2"
              id="inlineFormInput"
              placeholder="Wyszukaj film ..."
            />
          </div>
          <div class="col-auto" style={{ padding: 10 }}>
            <button type="submit" class="btn btn-primary mb-2 pull-right" onClick={this.ha}>
              szukaj
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default connect(
  state => state.searchForm,
)(SearchForm);
