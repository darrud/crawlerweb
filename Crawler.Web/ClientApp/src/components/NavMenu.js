﻿import React from "react";
import { Link } from "react-router-dom";
import { Glyphicon, Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import "./NavMenu.css";
import SearchForm from "./SearchForm";

export default props => (
  <Navbar inverse fixedTop fluid collapseOnSelect>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to={"/"}>FilmWeb Crawler</Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to={"/"} exact>
          <NavItem>
            <Glyphicon glyph="home" /> Strona Główna
          </NavItem>
        </LinkContainer>
        <LinkContainer to={"/movies"}>
          <NavItem>
            <Glyphicon glyph="expand" /> Filmy
          </NavItem>
        </LinkContainer>
        <LinkContainer to={"/people"} exact>
          <NavItem>
            <Glyphicon glyph="user" /> Aktorzy
          </NavItem>
        </LinkContainer>
      </Nav>
      <NavItem>
        <div>
        <SearchForm/>
        </div>
      </NavItem>
    </Navbar.Collapse>
  </Navbar>
);
