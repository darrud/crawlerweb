import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { actionCreators } from "../store/MovieDetails";

class MovieDetails extends Component {
  componentWillMount() {
    const movieId = parseInt(this.props.match.params.id, 10);
    this.props.requestMovieDetails(movieId);
    this.props.requestMoviePeople(movieId);
  }
  render() {
    return (
      <div style={{ padding: 50 }}>
        <div class="jumbotron">
          <h2 class="display-4">{this.props.details.title}</h2>
          <div style={{ height: 285 }} class="lead">
            <div class="pull-left">
              <p />
              <p>Kraj : {this.props.details.country}</p>
              <p>Rok produkcji : {this.props.details.premiere}</p>
              <p>Długość : {this.props.details.length}</p>
            </div>
            <img
              class="pull-right"
              src={this.props.details.poster}
              height="285"
              width="200"
            />
          </div>
          <hr class="my-4" />
          <div>
            <h3>Wystąpili :</h3>
            <br />
          </div>
          <div class="lead">
            <ul>
              {this.props.people.map(person => (
                <li>
                  <Link to={`/peopledetails/${person.personId}`}>
                    {person.name}
                  </Link>{" "}
                  jako {person.nameInMovie}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => state.movieDetails,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(MovieDetails);
