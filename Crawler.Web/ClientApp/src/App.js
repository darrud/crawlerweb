﻿import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import Movies from './components/Movies';
import People from './components/People';
import MovieDetails from './components/MovieDetails';
import PeopleDetails from './components/PeopleDetails';
import MoviesFiltered from './components/MoviesFiltered';

export default () => (
  <Layout>
    <Route exact path='/' component={Home} />
    <Route path='/movies/:pageIndex?' component={Movies} />
    <Route path='/movies3/:pageIndex?/:titleFilter?' component={MoviesFiltered} />
    <Route path='/people/:pageIndex?' component={People} />
    <Route path='/movie/details/:id?' component={MovieDetails} />
    <Route path='/peopledetails/:id?' component={PeopleDetails} />
  </Layout>
);
