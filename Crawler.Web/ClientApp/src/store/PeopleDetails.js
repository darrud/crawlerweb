const requestPeopleDetailsType = 'REQUEST_PEOPLE_DETAILS';
const receivePeopleDetailsType = 'RECEIVE_PEOPLE_DETAILS';
const requestPeopleMoviesType = 'REQUEST_PEOPLE_MOVIES_DETAILS';
const receivePeopleMoviesType = 'RECEIVE_PEOPLE_MOVIES_DETAILS';

const initialState = { details: {}, movies: [], isLoading: false };

export const actionCreators = {
    requestPeopleDetails: personId => async (dispatch, getState) => {    
      if (personId === getState().peopleDetails.personId) {
        return;
      }
  
      dispatch({ type: requestPeopleDetailsType, personId });
  
      const url = `api/People/Get/${personId}`;
      const response = await fetch(url);
      const details = await response.json();
  
      dispatch({ type: receivePeopleDetailsType, personId, details });
    },
    requestPeopleMoviesDetails: personId => async (dispatch, getState) => {    
      if (personId === getState().peopleDetails.personId) {
        return;
      }
  
      dispatch({ type: requestPeopleMoviesType, personId });
  
      const url = `api/Movies/GetPeopleMovies/${personId}`;
      const response = await fetch(url);
      const movies = await response.json();
  
      dispatch({ type: receivePeopleMoviesType, personId, movies });
    }
  };

  export const reducer = (state, action) => {
    state = state || initialState;
  
    if (action.type === requestPeopleDetailsType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        isLoading: true
      };
    }
  
    if (action.type === receivePeopleDetailsType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        details: action.details,
        isLoading: false
      };
    }

    if (action.type === requestPeopleMoviesType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        isLoading: true
      };
    }
  
    if (action.type === receivePeopleMoviesType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        movies: action.movies,
        isLoading: false
      };
    }
    return state;
  };