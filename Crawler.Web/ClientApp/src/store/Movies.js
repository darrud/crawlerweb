const requestMoviesType = 'REQUEST_MOVIES';
const receiveMoviesType = 'RECEIVE_MOVIES';
const requestMoviesFilteredType = 'REQUEST_MOVIES_FILTERED';
const receiveMoviesFilteredType = 'RECEIVE_MOVIES_FILTERED';

const initialState = { movies: [], isLoading: false };

export const actionCreators = {
    requestMovies: pageIndex => async (dispatch, getState) => {    
      if (pageIndex === getState().movies.pageIndex) {
        return;
      }
  
      dispatch({ type: requestMoviesType, pageIndex });
  
      const url = `api/Movies/GetAll/${pageIndex}/15`;
      const response = await fetch(url);
      const movies = await response.json();
  
      dispatch({ type: receiveMoviesType,pageIndex, movies });
    },

    requestMoviesFiltered: (pageIndex, titleFilter) => async (dispatch, getState) => {    
      if (pageIndex === getState().movies.pageIndex) {
        return;
      }
  
      dispatch({ type: requestMoviesFilteredType, pageIndex, titleFilter });
  
      const url = `api/Movies/GetAll/${pageIndex}/15/${titleFilter}`;
      const response = await fetch(url);
      const movies = await response.json();
  
      dispatch({ type: receiveMoviesFilteredType,pageIndex, movies });
    },

  };

  export const reducer = (state, action) => {
    state = state || initialState;
  
    if (action.type === requestMoviesType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        isLoading: true
      };
    }
  
    if (action.type === receiveMoviesType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        movies: action.movies,
        isLoading: false
      };
    }
    return state;
  };