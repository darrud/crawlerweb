const requestPeopleType = 'REQUEST_PEOPLE';
const receivePeopleType = 'RECEIVE_PEOPLE';

const initialState = { people: [], isLoading: false };

export const actionCreators = {
    requestPeople: pageIndex => async (dispatch, getState) => {    
      if (pageIndex === getState().people.pageIndex) {
        return;
      }
  
      dispatch({ type: requestPeopleType, pageIndex });
  
      const url = `api/People/GetAll/${pageIndex}/15`;
      const response = await fetch(url);
      const people = await response.json();
  
      dispatch({ type: receivePeopleType,pageIndex, people });
    }
  };

  export const reducer = (state, action) => {
    state = state || initialState;
  
    if (action.type === requestPeopleType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        isLoading: true
      };
    }
  
    if (action.type === receivePeopleType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        people: action.people,
        isLoading: false
      };
    }
    return state;
  };