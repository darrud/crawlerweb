const requestMovieDetailsType = 'REQUEST_MOVIE_DETAILS';
const receiveMovieDetailsType = 'RECEIVE_MOVIE_DETAILS';
const requestMoviePeopleType = 'REQUEST_MOVIE_PEOPLE';
const receiveMoviePeopleType = 'RECEIVE_MOVIE_PEOPLE';

const initialState = { details: {}, people: [], isLoading: false };

export const actionCreators = {
    requestMovieDetails: movieId => async (dispatch, getState) => {    
      if (movieId === getState().movieDetails.movieId) {
        return;
      }
  
      dispatch({ type: requestMovieDetailsType, movieId });
  
      const url = `api/Movies/Get/${movieId}`;
      const response = await fetch(url);
      const details = await response.json();
  
      dispatch({ type: receiveMovieDetailsType, movieId, details });
    },

    requestMoviePeople: movieId => async (dispatch, getState) => {    
      if (movieId === getState().movieDetails.movieId) {
        return;
      }
  
      dispatch({ type: requestMoviePeopleType, movieId });
  
      const url = `api/People/GetMoviePeople/${movieId}`;
      const response = await fetch(url);
      const people = await response.json();
  
      dispatch({ type: receiveMoviePeopleType, movieId, people });
    }
  };

  export const reducer = (state, action) => {
    state = state || initialState;
  
    if (action.type === requestMovieDetailsType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        isLoading: true
      };
    }
  
    if (action.type === receiveMovieDetailsType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        details: action.details,
        isLoading: false
      };
    }

    if (action.type === requestMoviePeopleType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        isLoading: true
      };
    }
  
    if (action.type === receiveMoviePeopleType) {
      return {
        ...state,
        pageIndex: action.pageIndex,
        people: action.people,
        isLoading: false
      };
    }
    return state;
  };