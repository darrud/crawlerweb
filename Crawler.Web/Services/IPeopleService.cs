﻿using System.Collections.Generic;
using Crawler.DAL.Models;
using Crawler.Web.Dtos;

namespace Crawler.Web.Services
{
    public interface IPeopleService
    {
        IEnumerable<Person> GetPaged(int pageNumber, int pageSize);
        IEnumerable<PeopleToMovieDto> GetMoviePeople(int id);
        Person GetById(int id);
    }
}
