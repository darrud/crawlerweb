﻿using System.Collections.Generic;
using Crawler.DAL.Models;
using Crawler.Web.Dtos;

namespace Crawler.Web.Services
{
    public interface IMoviesService
    {
        IEnumerable<Movie> GetPaged(int pageNumber, int pageSize);
        Movie GetById(int id);
        IEnumerable<MovieOnPeopleDetailsDto> GetPeopleMovies(int id);
        IEnumerable<Movie> GetFilteredByTitlePaged(int pageNumber, int pageSize, string title);
    }
}
