﻿using System.Collections.Generic;
using System.Linq;
using Crawler.DAL.Models;
using Crawler.DAL.Repositories;
using Crawler.Web.Dtos;

namespace Crawler.Web.Services
{
    public class PeopleService :IPeopleService
    {
        private readonly IPeopleRepository _peopleRepository;
        private readonly IPeopleToMovieRepository _peopleToMovieRepository;

        public PeopleService(IPeopleRepository peopleRepository, IPeopleToMovieRepository peopleToMovieRepository)
        {
            _peopleRepository = peopleRepository;
            _peopleToMovieRepository = peopleToMovieRepository;
        }

        public IEnumerable<Person> GetPaged(int pageNumber, int pageSize)
        {
            return _peopleRepository.GetAll().Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<PeopleToMovieDto> GetMoviePeople(int id)
        {
            var people = _peopleToMovieRepository.GetPeopleByMovieId(id).ToList();

            return people.Select(x =>
            {
                var personDetails = _peopleRepository.GetById(x.PersonId);
                return new PeopleToMovieDto(x.PersonId, x.MovieId, personDetails.Name, x.NameInMovie);
            }).ToList();


        }

        public Person GetById(int id)
        {
            return _peopleRepository.GetById(id);
        }
    }
}
