﻿using System.Collections.Generic;
using System.Linq;
using Crawler.DAL.Models;
using Crawler.DAL.Repositories;
using Crawler.Web.Dtos;

namespace Crawler.Web.Services
{
    public class MoviesService : IMoviesService
    {
        private readonly IMoviesRepository _moviesRepository;
        private readonly IPeopleToMovieRepository _peopleToMovieRepository;

        public MoviesService(IMoviesRepository moviesRepository, IPeopleToMovieRepository peopleToMovieRepository)
        {
            _moviesRepository = moviesRepository;
            _peopleToMovieRepository = peopleToMovieRepository;
        }

        public IEnumerable<Movie> GetPaged(int pageNumber, int pageSize)
        {
            return _moviesRepository.GetAll().Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public Movie GetById(int id)
        {
            return _moviesRepository.GetById(id);
        }

        public IEnumerable<MovieOnPeopleDetailsDto> GetPeopleMovies(int id)
        {
            var movies = _peopleToMovieRepository.GetMoviesByPersonId(id);

            return movies.Select(x =>
            {
                var moviesDetails = _moviesRepository.GetById(x.MovieId);

                return new MovieOnPeopleDetailsDto
                {
                    PersonId = x.PersonId,
                    NameInMovie = x.NameInMovie ?? string.Empty,
                    Title = moviesDetails?.Title ?? string.Empty
                };
            });
        }


        public IEnumerable<Movie> GetFilteredByTitlePaged(int pageNumber, int pageSize, string title)
        {
            return _moviesRepository.GetAll().Where(x => x.Title.Contains(title)).Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }
    }
}
