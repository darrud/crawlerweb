﻿using Crawler.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Crawler.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly IPeopleService _peopleService;

        public PeopleController(IPeopleService peopleService)
        {
            _peopleService = peopleService;
        }

        [HttpGet("GetAll/{pageNumber}/{pageSize}")]
        public IActionResult GetAll(int pageNumber, int pageSize)
        {

            return new JsonResult(_peopleService.GetPaged(pageNumber, pageSize));
        }

        [HttpGet("GetMoviePeople/{id}")]
        public IActionResult GetMoviePeople(int id)
        {
            return new JsonResult(_peopleService.GetMoviePeople(id));
        }

        [HttpGet("Get/{id}")]
        public IActionResult Get(int id)
        {
            return new JsonResult(_peopleService.GetById(id));
        }
    }
}