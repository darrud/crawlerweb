﻿using System.Linq;
using Crawler.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Crawler.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMoviesService _moviesService;

        public MoviesController(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }

        [HttpGet("GetAll/{pageNumber}/{pageSize}")]
        public IActionResult GetAll(int pageNumber, int pageSize)
        {
            
            return new JsonResult(_moviesService.GetPaged(pageNumber, pageSize));
        }

        [HttpGet("Get/{id}")]
        public IActionResult Get(int id)
        {
            return new JsonResult(_moviesService.GetById(id));
        }

        [HttpGet("GetPeopleMovies/{id}")]
        public IActionResult GetPeopleMovies(int id)
        {
            return new JsonResult(_moviesService.GetPeopleMovies(id).Where(x => x != null));
        }

        [HttpGet("GetAll/{pageNumber}/{pageSize}/{title}")]
        public IActionResult GetAll(int pageNumber, int pageSize, string title)
        {
            return new JsonResult(_moviesService.GetFilteredByTitlePaged(pageNumber, pageSize, title));
        }
    }
}