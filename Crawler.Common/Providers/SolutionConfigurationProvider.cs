﻿using Microsoft.Extensions.Configuration;

namespace Crawler.Common.Providers
{
    public class SolutionConfigurationProvider : ISolutionConfigurationProvider
    {
        private readonly IConfiguration _configuration;

        public SolutionConfigurationProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string GetSqlServerConnectionString()
        {
            return _configuration.GetConnectionString("SqlServer");
        }
    }
}
