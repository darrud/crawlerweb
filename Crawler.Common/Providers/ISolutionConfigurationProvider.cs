﻿namespace Crawler.Common.Providers
{
    public interface ISolutionConfigurationProvider
    {
        string GetSqlServerConnectionString();
    }
}
