﻿using Autofac;
using Crawler.Common.Providers;

namespace Crawler.Common.Ioc
{
    public class CommonModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SolutionConfigurationProvider>()
                .As<ISolutionConfigurationProvider>()
                .InstancePerLifetimeScope();
        }
    }
}
